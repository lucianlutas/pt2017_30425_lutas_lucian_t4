package ro.tuc.pt.asig4.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import ro.tuc.pt.asig4.model.Bank;

public class Serialization {
	
	public void serialize(Bank bank, String f) {
        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream(f))) {
            out.writeObject(bank);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Bank deSerialize(String f) {
        Bank bank = null;
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(f))) {
            bank = (Bank) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return bank;
    }
}
