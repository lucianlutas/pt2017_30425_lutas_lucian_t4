package ro.tuc.pt.asig4.ui;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import ro.tuc.pt.asig4.model.Account;
import ro.tuc.pt.asig4.model.Bank;
import ro.tuc.pt.asig4.model.Person;
import ro.tuc.pt.asig4.model.SavingAcc;
import ro.tuc.pt.asig4.model.SpendingAcc;
import ro.tuc.pt.asig4.serialization.Serialization;

public class Interface extends JFrame{

	private static final long serialVersionUID = 1L;
	private JButton btnAddAcc;
    private JButton btnAddMoney;
    private JButton btnRemAcc;
    private JButton btnRemoveMoney;
    private JButton btnUpdataAcc;
    private JPanel jPanel1;
    private JScrollPane scrPanel;
    private JLabel lblClientID;
    private JLabel lblMoney;
    private JLabel lblMoneyToAdd;
    private JLabel lblName;
    private JLabel lblNo;
    private JLabel lblPhone;
    private JLabel lblSavings;
    private JRadioButton savingAcc;
    private JRadioButton spendingAcc;
    private JTable tblClients;
    private JTextField txtID;
    private JTextField txtMoney;
    private JTextField txtMoneyToAdd;
    private JTextField txtName;
    private JTextField txtNo;
    private JTextField txtPhone;
    private DefaultTableModel model;
    private ButtonGroup grpType;
    private Serialization ser = new Serialization();
    private Bank bank = new Bank();
    
    public Interface(){
    	jPanel1 = new JPanel();
        scrPanel = new JScrollPane();
        lblClientID = new JLabel();
        txtID = new JTextField();
        lblName = new JLabel();
        txtName = new JTextField();
        txtPhone = new JTextField();
        lblPhone = new JLabel();
        lblNo = new JLabel();
        txtNo = new JTextField();
        lblMoney = new JLabel();
        txtMoney = new JTextField();
        lblSavings = new JLabel();
        btnAddAcc = new JButton();
        btnRemAcc = new JButton();
        btnUpdataAcc = new JButton();
        savingAcc = new JRadioButton();
        spendingAcc = new JRadioButton();
        btnAddMoney = new JButton();
        btnRemoveMoney = new JButton();
        lblMoneyToAdd = new JLabel();
        txtMoneyToAdd = new JTextField();
        tblClients = new JTable(){
        	private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {                
                    return false;               
            };
        };
        
        //ser.serialize(bank, "Bank.ser");

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        scrPanel.setViewportView(tblClients);
        
        bank = ser.deSerialize("Bank.ser");
        model = populate(bank);
        tblClients.setModel(model);
        
        tblClients.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	tblClientsMouseClicked(evt);
            }
        });

        lblClientID.setText("Client ID");

        lblName.setText("Full Name");

        lblPhone.setText("Phone");

        lblNo.setText("Acc No");

        lblMoney.setText("Balance");

        lblSavings.setText("Type");

        btnAddAcc.setText("Create");
        btnAddAcc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddAccActionPerformed(evt);
            }
        });

        btnRemAcc.setText("Delete");
        btnRemAcc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemAccActionPerformed(evt);
            }
        });

        btnUpdataAcc.setText("Update");
        btnUpdataAcc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdataAccActionPerformed(evt);
            }
        });
        
        grpType = new ButtonGroup();
        savingAcc.setText("Savings");
        spendingAcc.setText("Spending");
        
        grpType.add(savingAcc);
        grpType.add(spendingAcc);

        btnAddMoney.setText("Add Money");
        btnAddMoney.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMoneyActionPerformed(evt);
            }
        });

        btnRemoveMoney.setText("Remove Money");
        btnRemoveMoney.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	btnRemoveMoneyActionPerformed(evt);
            }
        });

        lblMoneyToAdd.setText("Money");

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(scrPanel, GroupLayout.PREFERRED_SIZE, 595, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(lblClientID)
                            .addComponent(lblName))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(txtName)
                            .addComponent(txtID)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(lblNo)
                            .addComponent(lblPhone))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(txtPhone)
                            .addComponent(txtNo)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(lblMoney)
                            .addComponent(lblSavings))
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(savingAcc)
                                .addGap(18, 18, 18)
                                .addComponent(spendingAcc)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(txtMoney))))
                    .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(lblMoneyToAdd)
                        .addGap(32, 32, 32)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnAddAcc)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRemAcc)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnUpdataAcc))
                            .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnAddMoney)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRemoveMoney))
                            .addComponent(txtMoneyToAdd, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(scrPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblClientID, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPhone, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPhone, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNo, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMoney, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMoney, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(savingAcc)
                    .addComponent(spendingAcc)
                    .addComponent(lblSavings, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddAcc)
                    .addComponent(btnRemAcc)
                    .addComponent(btnUpdataAcc))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMoneyToAdd)
                    .addComponent(txtMoneyToAdd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddMoney)
                    .addComponent(btnRemoveMoney))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 15, Short.MAX_VALUE)
                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 15, Short.MAX_VALUE))
        );

        pack();
        this.setTitle("Bank Application");
        this.setVisible(true);
    }

	protected void tblClientsMouseClicked(MouseEvent evt) {
		int i = tblClients.getSelectedRow();
        TableModel model = tblClients.getModel();
        txtID.setText(model.getValueAt(i,0).toString());
        txtName.setText(model.getValueAt(i, 2).toString());
        txtPhone.setText(model.getValueAt(i, 3).toString());
        txtNo.setText(model.getValueAt(i, 1).toString());
        txtMoney.setText(model.getValueAt(i, 5).toString());
        if(model.getValueAt(i, 4).toString() == "Savings Account")
        {
        	savingAcc.setSelected(true);
        	spendingAcc.setSelected(false);
        }
        else if(model.getValueAt(i, 4).toString() == "Spending Account"){
        	savingAcc.setSelected(false);
        	spendingAcc.setSelected(true);
        }
		
	}

	protected void btnRemoveMoneyActionPerformed(ActionEvent evt) {
		try {
            bank = ser.deSerialize("Bank.ser");
            Account acc = bank.getAcc(Integer.parseInt(txtNo.getText()));
            bank.removeMoney(acc, Double.parseDouble(txtMoneyToAdd.getText()));
            ser.serialize(bank, "Bank.ser");
            model.setRowCount(0);
            model = populate(bank);
            tblClients.setModel(model);
        } catch (Exception ex) {
            ex.printStackTrace();
            infoBox("Error", "Can't remove money from account");
        }
		
	}

	protected void btnAddMoneyActionPerformed(ActionEvent evt) {
		try {
            bank = ser.deSerialize("Bank.ser");
            Account acc = bank.getAcc(Integer.parseInt(txtNo.getText()));
            bank.addMoney(acc, Double.parseDouble(txtMoneyToAdd.getText()));
            ser.serialize(bank, "Bank.ser");
            model.setRowCount(0);
            model = populate(bank);
            tblClients.setModel(model);
        } catch (Exception ex) {
            ex.printStackTrace();
            infoBox("Error", "Can't add money to account");
        }
		
	}

	protected void btnUpdataAccActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		
	}

	protected void btnRemAccActionPerformed(ActionEvent evt) {
		try {
            bank = ser.deSerialize("Bank.ser");
            Account acc = bank.getAcc(Integer.parseInt(txtNo.getText()));
            bank.removeAcc(acc);
            ser.serialize(bank, "Bank.ser");
            model.setRowCount(0);
            model = populate(bank);
            tblClients.setModel(model);
        } catch (Exception ex) {
            ex.printStackTrace();
            infoBox("Error", "No Account");
        }
		
	}

	protected void btnAddAccActionPerformed(ActionEvent evt) {
		try {
            bank = ser.deSerialize("Bank.ser");
            Person person = new Person();
            person.setID(Integer.parseInt(txtID.getText()));
            person.setName(txtName.getText());
            person.setPhone(txtPhone.getText());
            int accNo = Integer.parseInt(txtNo.getText());
            //Account acc = new Account();
            if (savingAcc.isSelected() == true) {
                SavingAcc sa = new SavingAcc();
                sa.setPerson(person);
                sa.setMoney(Double.parseDouble(txtMoney.getText()));
                for(Entry<Person, ArrayList<Account>> entry : bank.getBank().entrySet()){
                	for(int i = 0; i < entry.getValue().size(); i++){
                		if(entry.getValue().get(i).getNo() == accNo){
                			if(entry.getValue().get(i).getSavingsAcc() == "Savings Account"){
                				accNo++;
                			}
                		}
                	}
                }
                sa.setNo(accNo);
                bank.addAcc(sa);
            } else if (spendingAcc.isSelected() == true) {
                SpendingAcc sa = new SpendingAcc();
                sa.setPerson(person);
                sa.setMoney(Double.parseDouble(txtMoney.getText()));
                for(Entry<Person, ArrayList<Account>> entry : bank.getBank().entrySet()){
                	for(int i = 0; i < entry.getValue().size(); i++){
                		if(entry.getValue().get(i).getNo() == accNo){
                			if(entry.getValue().get(i).getSavingsAcc() == "Spending Account"){
                				accNo++;
                			}
                		}
                	}
                }
                sa.setNo(accNo);
                bank.addAcc(sa);
            }
            ser.serialize(bank, "Bank.ser");
            txtID.setText("");
            txtName.setText("");
            txtPhone.setText("");
            txtNo.setText("");
            txtMoney.setText("");
            model.setRowCount(0);
            model = populate(bank);
            tblClients.setModel(model);
        } catch (Exception ex) {
            ex.printStackTrace();
            infoBox("Error", "Wrong Data");
        }		
	}
	
	private DefaultTableModel populate(Bank bank){
		DefaultTableModel model = new DefaultTableModel();
		model = new DefaultTableModel(null, new Object[] { "ID", "AccNo",
                "FullName", "Phone", "Type", "Balance" });
		for (Entry<Person, ArrayList<Account>> entry : bank.getBank().entrySet()) {
            for(int i = 0; i< entry.getValue().size(); i++){
            	Account acc = entry.getValue().get(i);
                model.addRow(new Object[] { 
                	acc.getPerson().getID(),
                	acc.getNo(),
                	acc.getPerson().getName(), 
                	acc.getPerson().getPhone(), 
                	acc.getSavingsAcc(),
                	acc.getMoney() 
                });
            }
        }
		
		return model;
	}
	
	public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage,titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
}
