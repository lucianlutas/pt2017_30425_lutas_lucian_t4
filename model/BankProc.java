package ro.tuc.pt.asig4.model;

public interface BankProc {
	/*
	 * @pre 
	 * @post
	 */
    public Account getAcc(Person pers, int no);
    public void addAcc(Account acc);
    public void removeAcc(Account acc);
    public void addMoney(Account acc, double money);
    public void removeMoney(Account acc, double money);
}
