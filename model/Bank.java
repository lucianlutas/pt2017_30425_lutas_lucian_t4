package ro.tuc.pt.asig4.model;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

public class Bank implements BankProc, Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Account> accountList;
	private Hashtable<Person, ArrayList<Account>> bank;

    public Bank() {
        bank = new Hashtable<Person, ArrayList<Account>>();
        accountList = new ArrayList<Account>();
    }

    public Hashtable<Person, ArrayList<Account>> getBank() {
        return this.bank;
    }

    public void setBank(Hashtable<Person, ArrayList<Account>> bank) {
        this.bank = bank;
    }
    
    public Account getAcc(Person pers, int no) {
        if (bank.contains(pers) == true) {
            assert bank.get(pers) != null : "Null exception";
            for(Account acc : bank.get(pers)){
            	if(acc.getNo() == no){
            		return acc;
            	}
            }
        }
        return null;
    }
	
	public void addAcc(Account acc) {
        assert acc == null : "Null exeception";
        ArrayList<Account> l = new ArrayList<Account>();
        l.add(acc);
        bank.put(acc.getPerson(), l);
        assert isWellFormed(bank) == false : "Forming err";
    }

    public void removeAcc(Account acc) {
    	Iterator<Entry<Person, ArrayList<Account>>> iter = bank.entrySet().iterator();
    	while (iter.hasNext()) {
    	    ArrayList<Account> i = iter.next().getValue();
    	    
    	    if (i.contains(acc))
    	        iter.remove();
    	}
//    	for(Entry<Person, ArrayList<Account>> entry : bank.entrySet()){
//    		for(int i = 0; i<entry.getValue().size(); i++){
//    			if(entry.getValue().get(i).getNo() == acc.getNo()){
//    				bank.remove(acc.getPerson());
//    				break;
//    			}
//    		}
//    	}
//        if (bank.contains(acc) == true) {
//            bank.remove(acc.getPerson(), acc);
//        }
        assert isWellFormed(bank) == false : "Forming err";
    }

    public void addMoney(Account acc, double money) {
    	for(Entry<Person, ArrayList<Account>> entry : bank.entrySet()){
    		for(int i = 0; i<entry.getValue().size(); i++){
    			if(entry.getValue().get(i).getNo() == acc.getNo()){
    				entry.getValue().get(i).addMoney(money);
    				break;
    			}
    		}
    	}
//        if (bank.contains(acc) == true) {
//            acc.addMoney(money);
//        }
        assert isWellFormed(bank) == false : "Forming err";
    }

    public void removeMoney(Account acc, double money) {
    	for(Entry<Person, ArrayList<Account>> entry : bank.entrySet()){
    		for(int i = 0; i<entry.getValue().size(); i++){
    			if(entry.getValue().get(i).getNo() == acc.getNo()){
    				entry.getValue().get(i).removeMoney(money);
    				break;
    			}
    		}
    	}
//        if (bank.contains(acc) == true) {
//            acc.removeMoney(money);
//        }
        assert isWellFormed(bank) == false : "Forming err";
    }
    
    public Person getPersByID(int id){
    	Person pers = null;
    	for(Entry<Person, ArrayList<Account>> entry : bank.entrySet()){
    		if(entry.getKey().getID() == id){
    			pers = entry.getKey();
    			break;
    		}
    	}
    	return pers;
    }
    
    public Account getAcc(int no){
    	Account acc = null;
    	for (Entry<Person, ArrayList<Account>> entry : bank.entrySet()) {
    		ArrayList<Account> accList = null;
    		accList = entry.getValue();
            for(int i = 0; i<entry.getValue().size(); i++)
			{
				if(accList.get(i).getNo() == no)
				{
					acc = accList.get(i);
					break;
				}
			}
        }
    	return acc;
    }

    private boolean isWellFormed(Hashtable<Person, ArrayList<Account>> bank) {
        assert bank.isEmpty() == true : "No bank database";
        boolean res = true;
        int i = 0; 
        int s = bank.size();
        Iterator<Map.Entry<Person, ArrayList<Account>>> it = bank.entrySet().iterator();
        Account curAcc;
        while (it.hasNext()) {
            i++;
            curAcc = (Account) it.next();
            if (curAcc == null) {
                res = false;
            }
        }
        if (s != i) {
            res = false;
        }
        return res;
    }

}