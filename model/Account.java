package ro.tuc.pt.asig4.model;

import java.io.Serializable;

public abstract class Account implements Serializable {

	private static final long serialVersionUID = 1L;
	private int no;
    private Person pers;
    private double money;
    
    public int getNo() {
        return this.no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Person getPerson() {
        return this.pers;
    }

    public void setPerson(Person pers) {
        this.pers = pers;
    }

    public double getMoney() {
        return this.money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
    public double addMoney(double moneyToAdd){
   	 if (this.money + moneyToAdd < 0) {
            return -1;
        }
   	 this.money = this.money + moneyToAdd;
        return this.money;
   }

   public double removeMoney(double moneyToRemove){
   	if (this.money < moneyToRemove) {
           return -1;
       }
   	this.money = this.money - moneyToRemove;
       return this.money;
   }
   
   public abstract String getSavingsAcc();
}
