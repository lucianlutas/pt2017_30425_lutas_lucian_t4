package ro.tuc.pt.asig4.model;

import java.io.Serializable;


public class Person implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
    private String name;
    private String phone;

    public int getID() {
        return this.id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }



}
