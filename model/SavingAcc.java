package ro.tuc.pt.asig4.model;

import java.io.Serializable;

public class SavingAcc extends Account implements Serializable{
	private static final long serialVersionUID = 1L;

    public String getSavingsAcc(){
        return "Savings Account";
    }

}
